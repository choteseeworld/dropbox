// Define pin sensor middle and 2 check card sensor
int sensor_middle = 2;
int senser_checkcard1 = 3;
int senser_checkcard2 = 8;

// Define moter constant and enable pin
int motorA = 4;
int motorB = 5;
int EN = 6;

// Define constant sensor read
int analog_input;
int volumn_analog;
int read_sensor_middle;
int read_sensor_checkcard1;
int read_sensor_checkcard2;

void setup() {
  Serial.begin(9600);

  pinMode(sensor_middle, INPUT);
  pinMode(senser_checkcard1, INPUT);
  pinMode(senser_checkcard2, INPUT);

  pinMode(motorA, OUTPUT);
  pinMode(motorB, OUTPUT);
  pinMode(EN, OUTPUT);

  // Read analog of potentiometer -> Adjust time to delay motor
  analog_input = analogRead(A0);
  volumn_analog = map(analog_input, 0, 1023, 0, 1000); //ปรับค่า analog จาก volumn เพื่อปรับเวลาของการหน่วงก่อนปัดบตรทิ้ง 0-5 วินาที

  // เช็คสถานะแกนมาเตอร์ว่าอยู่ตรงกลางหรือไม่
  read_sensor_middle = digitalRead(sensor_middle);
  if (read_sensor_middle != 0) {
    //Serial.print("In setup: ");
    Serial.println(read_sensor_middle);
    attachInterrupt(digitalPinToInterrupt(sensor_middle), Swipe_stop, FALLING);
    digitalWrite(motorA, HIGH);
    digitalWrite(motorB, LOW);
    analogWrite(EN, 1023);
  }
  //Serial.println("Close While Loop");
}

void loop() {
  read_sensor_middle = digitalRead(sensor_middle);
  read_sensor_checkcard1 = digitalRead(senser_checkcard1);
  read_sensor_checkcard2 = digitalRead(senser_checkcard2);

  //เช็คเซ็นเซอร์ทั้งหมดเพื่อหยุดการ interrupt ไว้ก่อน มอเตอร์จะได้หมุนได้โดยไม่ต้องไปทำ interrupt เพื่อหยุดมอเตอร์
  if (read_sensor_middle == 0) {
    detachInterrupt(digitalPinToInterrupt(sensor_middle));
  }
  if ( read_sensor_checkcard1 == 0 || read_sensor_checkcard2 == 0) {
    detachInterrupt(digitalPinToInterrupt(sensor_middle));
    delay(volumn_analog);
    //Serial.println("Have Card");

    //เริ่ม -> มอเตอร์ปัดบัตร
    Swipe_card();

    // มอเคอร์ปัดอีก 1 รอบ เพื่อความชัวร์
    Swipe_card();
  }
  
}

void Swipe_card() { //มอเตอร์หมุนไปข้างหน้าเพื่อปัดบัตร
  digitalWrite(motorA, LOW);
  digitalWrite(motorB, HIGH);
  analogWrite(EN, 1023);
  delay(600);
  attachInterrupt(digitalPinToInterrupt(sensor_middle), Swipe_stop, FALLING);
  //delay(200);
  //Serial.println("Swipe_card");
  Swipe_reverse();

}

void Swipe_stop() { //หยุดมอเตอร์
  digitalWrite(motorA, LOW);
  digitalWrite(motorB, LOW);
  //Serial.println("stop");
  //  delay(200);

}

void Swipe_reverse() { //มอเตอร์หมุนย้อนกลับ เพื่อคงตำแหน่งเดิมของจุด home
  digitalWrite(motorA, HIGH);
  digitalWrite(motorB, LOW);
  //analogWrite(EN, 1000);
  delay(800);
  //Serial.println("reverse");
}
